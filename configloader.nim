import httpclient, parsecfg, uri, os, parseopt


var client* {.threadvar.}: HttpClient
client = newHttpClient(timeout = 65000)
client.headers = newHttpHeaders({"Content-Type": "application/json"})

type Config* = object
  location*: string
  roomId*: string
  token*: string
  baseUrl*: string
  baseMediaUrl*: string

proc load(c: var Config) =
  if fileExists(c.location):
    let rawConfig = loadConfig(c.location)
    c.roomId = encodeUrl(rawConfig.getSectionValue("", "roomId"))
    c.token = rawConfig.getSectionValue("", "token")
    c.baseUrl = rawConfig.getSectionValue("", "baseUrl")
    c.baseMediaUrl = rawConfig.getSectionValue("", "baseUrl") # Is this even different?

let f = getCurrentDir() & "/config.ini"
var conf* = if f.fileExists: Config(location: f) else: Config(location: getConfigDir() &
    "./nimbot/config.ini")
conf.load()

proc checkConfig*(baseUrl: string, roomId: string, token: string): bool =
  echo "looking for config at: " & getConfigDir() & "./nimbot/config.ini"
  if baseUrl == "":
    echo "Please provide a base URL. Either through a config file or as a command line option"
  elif roomId == "":
    echo "Please provide a room ID. Either through a config file or as a command line option"
  elif token == "":
    echo "Please provide an access token. Either through a config file or as a command line option"
  else:
    return true

var message*: string
var file*: string
var html* = false

var p = initOptParser()
while true:
  p.next()
  case p.kind
  of cmdEnd: break
  of cmdShortOption:
    if p.key == "f":
      file = p.val
    if p.key == "m":
      message = p.val
    if p.key == "t":
      conf.token = p.val
    if p.key == "r":
      conf.roomId = p.val
    if p.key == "u":
      conf.baseUrl = p.val
  of cmdLongOption:
    if p.key == "html":
      html = true
  of cmdArgument:
    message = p.key
