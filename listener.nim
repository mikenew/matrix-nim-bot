import httpclient, json, parsecfg, uri, parseopt, os, strutils
import configloader

type Message* = object
  body*: string
  roomId*: string

type SyncResponse = object
  since: string
  messages: seq[Message]

proc getLatestMessage*(since = "", baseUrl: string = conf.baseUrl, roomId: string = conf.roomId, token: string = conf.token): SyncResponse =
  if checkConfig(baseUrl, roomId, token):
    # seems to error if we try to wait a full 60 seconds
    let u = baseUrl & "/_matrix/client/v3/sync?access_token=" & token & "&timeout=45000" & 
      (if not since.isEmptyOrWhitespace: "&since=" & since else: "")

    let response = client.request(u, httpMethod = HttpGet)
    echo response.status
    echo response.body

    let js = parseJson(response.body)

    if js.hasKey("rooms") and js["rooms"].hasKey("join"):
      for key, value in js{"rooms"}{"join"}:
        #echo key # room id
        for e in value{"timeline"}{"events"}:
          let t = e{"type"}.getStr.strip()
          if t == "m.room.message":
            var m = Message()
            m.body = e{"content"}{"body"}.getStr
            m.roomId = key
            result.messages.add m

    result.since = js{"next_batch"}.getStr

proc pollForMessages*(token = conf.token, onMessage: proc(message: Message)) =
  var since = ""
  while true:
    let r = getLatestMessage(since, token = token)

    # we don't want to deal with backfill. this skips the first sync
    if not since.isEmptyOrWhitespace:
      for m in r.messages:
        onMessage(m)

    # if the request times out, we don't get a new 'since' value
    if not r.since.isEmptyOrWhitespace: since = r.since

    os.sleep(1000)

