import std/[strutils, osproc, asyncdispatch]
import bot

const tkn = "syt_b3dsYm90_zifIdNUrgkbFqMhqSGKx_1HYEHb"

proc speak(message: string, roomId: string) {.async.} =
  let ttspath = "/home/mikenew/projects/nim/workout_audio_generator/tortoise-tts"
  # TODO: probably safer to just write to a file an read it. That's what the script 
  # does anyway

  let r = execCmdEx("cd " & ttspath & " ; ./read_fast.sh \"" & message & "\"")
  if r.exitCode == 0:
    echo "tts ran successfully"
    echo r.output
    sendAudioMessage("/tmp/tortoise/out.opus", message = "tts result", roomId = roomId, token = tkn)
  else:
    echo "error in tts generation"

proc summarize(url: string, roomId: string) {.async.} =
  let r = execCmdEx("/usr/local/bin/transcribe_url " & url)
  if r.exitCode == 0:
    echo "STT ran successfully"
    echo r.output
    sendMessage(r.output, roomId = roomId, token = tkn)
  else:
    echo "error in tts generation"

pollForMessages(token = tkn, proc(m: Message) = 
  if m.body.startsWith("!speak"):
    let clean = m.body.replace("!speak", "").strip()
    echo "speaking message: " & clean

    asyncCheck speak(clean, m.roomId) # runs asyncronously so we don't block the polling

  elif m.body.startsWith("!ping"):
    sendMessage("ping", roomId = m.roomId, token = tkn)

  elif m.body.startsWith("!summarize"):
    let clean = m.body.replace("!summarize", "").strip()
    asyncCheck summarize(clean, roomId = m.roomId)

  elif m.body.startsWith("!up"):
    sendAudioMessage("/tmp/tortoise/out.opus", message = "tts result", roomId = m.roomId, token = tkn)
)

# FIXME: pretty sure we need to put these execCmdEx calls on a background thread
