import httpclient, json, strutils, os, asyncdispatch
import configloader, listener
export listener

proc printHelp =
  echo "you did it wrong"

proc getToken =
  let u = conf.baseUrl & "/_matrix/client/r0" & "/login"

  let body = %*{
      "type": "m.login.password",
      "user": "nimbot",
      "password": "password123"
  }

  let response = client.request(u, httpMethod = HttpPost, body = $body)
  echo response.status
  echo response.body

proc getClient(): Httpclient =
  # In a multithreaded environment, client might not be initialized
  # Pretty sure spawning a thread *should* run the initializer, but mummy threads are weird

  if client == nil:
    client = newHttpClient(timeout = 65000)
    client.headers = newHttpHeaders({"Content-Type": "application/json"})
  return client

proc createRoom =
  let u = conf.baseUrl & "/_matrix/client/r0" & "/createRoom?access_token=" & conf.token

  let body = %*{
      "room_alias_name": "bot_room1"
  }

  let response = client.request(u, httpMethod = HttpPost, body = $body)
  echo response.status
  echo response.body

proc sendMessage*(text: string, html = false, baseUrl: string = conf.baseUrl, roomId: string = conf.roomId, token: string = conf.token) =
  if checkConfig(baseUrl, roomId, token):
    let u = baseUrl & "/_matrix/client/r0" & "/rooms/" & roomId & "/send/m.room.message?access_token=" & token

    var body = %*{
        "msgtype": "m.text",
        "body": text
    }
    if html:
      body["format"] = %*"org.matrix.custom.html"
      # TODO: strip hmtl?
      body["formatted_body"] = %*text

    let response = getClient().request(u, httpMethod = HttpPost, body = $body)
    echo response.status

proc sendMessageToMultiple*(text: string, html = false, baseUrl: string = conf.baseUrl, roomIds: seq[string], token: string = conf.token) =
  for rId in roomIds:
    bot.sendMessage(text, html, baseUrl, rId, token)


proc uploadFile(file: string, mimetype: string, baseUrl: string = conf.baseUrl, token: string = conf.token): string =
  # returns the internal matrix media link
  let uploadClient = newHttpClient()
  uploadClient.headers = newHttpHeaders({"Content-Type": mimetype})
  let n = file.splitFile.name

  # TOTO: make sure that it's okay to use baseUrl vs 'baseMediaUrl"
  let u = baseUrl & "/_matrix/media/v3/upload?filename=" & n & "&access_token=" & token

  let file = readFile(file)
  let response = uploadClient.request(u, httpMethod = HttpPost, body = $file)
  echo response.body
  let jsonNode = parseJson(response.body)
  return jsonNode["content_uri"].getStr

proc uploadImageFile*(file: string, baseUrl: string = conf.baseUrl, token: string = conf.token): string =
  return uploadFile(file, "image/png", baseUrl, token)

proc uploadAudioFile*(file: string): string =
  return uploadFile(file, "audio/ogg")

proc sendAudioMessage*(file: string, message: string = "", roomId: string = conf.roomId, token: string = conf.token) =
  if not file.fileExists:
    echo "File doesn't exist"
  else:
    let url = uploadAudioFile(file)

    let u = conf.baseUrl & "/_matrix/client/r0" & "/rooms/" & roomId & "/send/m.room.message?access_token=" & token

    let body = %*{
        "msgtype": "m.audio",
        "body": message,
        "url": url
    }

    let response = client.request(u, httpMethod = HttpPost, body = $body)
    echo response.status

proc sendImageMessage*(file: string, message: string = "", baseUrl: string = conf.baseUrl, roomId: string = conf.roomId, token: string = conf.token) =
  if not file.fileExists:
    echo "File doesn't exist"
  else:
    let url = uploadImageFile(file, baseUrl, token)

    let body = %*{
        "msgtype": "m.image",
        "body": message,
        "url": url
    }

    let u = baseUrl & "/_matrix/client/r0/rooms/" & roomId & "/send/m.room.message?access_token=" & token
    let response = getClient().request(u, httpMethod = HttpPost, body = $body)
    echo response.status

when isMainModule:
  #if file.isNotEmptyOrWhitespace:
  #  Doesn't make sense to blindly assume it's an image
  #  let r = uploadImage(file)
  #  sendImageMessage(file, r, message)
  if message.len > 0:
    sendMessage(message, html)
